const User = require('../models/user');
const {
  getDbConnection,
  Sequelize,
  Model
} = require('../utils/db');

class Cake extends Model {}
Cake.init({
  name: {
    type: Sequelize.STRING
  },
  user_id: {
    type: Sequelize.INTEGER
  },
  link: {
    type: Sequelize.STRING
  },
  lat: {
    type: Sequelize.FLOAT
  },
  lon: {
    type: Sequelize.FLOAT
  },
  locationName: {
    type: Sequelize.STRING
  },
  description: {
    type: Sequelize.STRING,
    validate: {
      len: [0, 300]
    }
  }
}, {
  sequelize: getDbConnection(),
  modelName: 'cakes',
  timestamps: true,
  underscored: true
});

User.hasMany(Cake, {constraints: true});
Cake.belongsTo(User, {constraints: true});

Cake.sync();

module.exports = Cake;