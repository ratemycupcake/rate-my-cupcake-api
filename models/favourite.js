const Cake = require('../models/cake');
const User = require('../models/user');
const {
  getDbConnection,
  Sequelize,
  Model
} = require('../utils/db');

class Favourite extends Model {}
Favourite.init({
  user_id: {
    type: Sequelize.INTEGER,
    unique: 'fav'
  },
  cake_id: {
    type: Sequelize.INTEGER,
    unique: 'fav'
  },
}, {
  sequelize: getDbConnection(),
  modelName: 'favourites',
  timestamps: true,
  underscored: true
});

Cake.hasMany(Favourite, {constraints: true});
User.hasMany(Favourite, {constraints: true});
Favourite.belongsTo(Cake, {constraints: true});

Favourite.sync();

module.exports = Favourite;
