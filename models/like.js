const {
  getDbConnection,
  Sequelize,
  Model
} = require('../utils/db');

class Like extends Model {}
Like.init({
  user_id: {
    type: Sequelize.INTEGER,
    unique: 'like'
  },
  rating_id: {
    type: Sequelize.INTEGER,
    unique: 'like'
  },
  reply_id: {
    type: Sequelize.INTEGER,
    unique: 'like'
  },
}, {
  sequelize: getDbConnection(),
  modelName: 'likes',
  timestamps: true
});
Like.sync();

module.exports = Like;
