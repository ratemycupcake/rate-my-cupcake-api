const {
    getDbConnection,
    Sequelize,
    Model
} = require('../utils/db');

class Rating extends Model {}
Rating.init({
  user_id: {
    type: Sequelize.STRING,
    unique: 'uniqueRating'
  },
  rating: {
    type: Sequelize.INTEGER
  },
  cake_id: {
    type: Sequelize.INTEGER,
    unique: 'uniqueRating'
  },
  comment: {
    type: Sequelize.STRING,
  }
}, {
  sequelize: getDbConnection(),
  modelName: 'ratings',
  timestamps: true
});
Rating.sync();

module.exports = Rating;
