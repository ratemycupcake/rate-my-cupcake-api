const {
  getDbConnection,
  Sequelize,
  Model
} = require('../utils/db');

class Reply extends Model {}
Reply.init({
  user_id: {
    type: Sequelize.INTEGER,
  },
  rating_id: {
    type: Sequelize.INTEGER
  },
  reply_id: {
    type: Sequelize.INTEGER,
  },
  comment: {
    type: Sequelize.STRING,
  }
}, {
  sequelize: getDbConnection(),
  modelName: 'replies',
  timestamps: true
});
Reply.sync();

module.exports = Reply;
