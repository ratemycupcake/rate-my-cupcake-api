const {
    getDbConnection,
    Sequelize,
    Model
} = require('../utils/db');

class User extends Model {}
User.init({
  email: {
    type: Sequelize.STRING,
    unique: true
  },
  display_name: {
    type: Sequelize.STRING,
    unique: true
  },
  password: {
    type: Sequelize.STRING
  },
  avatar: {
    type: Sequelize.STRING
  }
}, {
  sequelize: getDbConnection(),
  modelName: 'users',
  timestamps: true
});
User.sync();

module.exports = User;