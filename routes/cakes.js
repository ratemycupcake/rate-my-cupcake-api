const express = require('express');
const Cake = require('../models/cake');
const User = require('../models/user');
const Rating = require('../models/rating');
const Favourite = require('../models/favourite');
const router = express.Router();

router.get('/', async function (req, res, next) {
  Cake.findAll().then(async cakes => {
    const promises = [];
    cakes.forEach(cake => {
      promises.push(Rating.findAll({
        where: {
          cake_id: cake.id
        }
      }).then(ratings => {
        if (cake) {
          cake = JSON.parse(JSON.stringify(cake, null, 2));
          cake.ratings = JSON.parse(JSON.stringify(ratings, null, 2));
        }
      }))
    });
    await Promise.all(promises);
    res.status(200).send(cakes);
  });
});

router.get('/top', function (req, res, next) {
  Cake.findAll().then(cakes => {
    res.status(200).send(cakes);
  });
});

router.post('/', function(req, res, next) {
  const input = req.body;
  Cake.create(input);
  
  res.status(200).send();
});

router.post('/:id', function(req, res, next) {
  const id = req.params.id;
  const input = req.body;
  Cake.findOne({
    where: {
      id
    },
    include: [{
      model: User
    }]
  }).then(cake => {
    Rating.findAll({
      where: {
        cake_id: cake.id
      }
    }).then(async ratings => {
      if (cake) {
        cake = JSON.parse(JSON.stringify(cake, null, 2));
        const promises = [];
        
        ratings.forEach(rating => {
          promises.push(User.findOne({
            where: {
              id: rating.user_id,
            }
          }).then(user => {
            rating.setDataValue('display_name', user.display_name);
            rating.setDataValue('avatar', user.avatar);
          }));
        });

        await Promise.all(promises);
        cake.ratings = JSON.parse(JSON.stringify(ratings, null, 2));
        if (input.user_id) {
          Favourite.count({
            where: {
              cake_id: cake.id,
              user_id: input.user_id
            }
          }).then(fav => {
            if (fav) {
              cake.favourited = fav;
            }
            res.status(200).send(cake);
          });
        } else {
          res.status(200).send(cake);
        }
      } else {
        res.status(404).send();
      }
    })
  });
});

router.post('/:id/favourite', function(req, res, next) {
  const input = req.body;
  const id = req.params.id;
  Favourite.count({
    where: {
      user_id: input.user_id,
      cake_id: id
    }
  }).then(count => {
    if (count === 1) {
      Favourite.destroy({
        where: {
          user_id: input.user_id,
          cake_id: id
        }
      });
      res.status(200).send({});
    } else {
      Favourite.create({
        user_id: input.user_id,
        cake_id: id
      });
      res.status(200).send({});
    }
  })
});

module.exports = router;
