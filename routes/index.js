var express = require('express');
require('dotenv').config();
var router = express.Router();

const Cake = require('../models/cake');
const Rating = require('../models/rating');
const { Op } = require('../utils/db');

router.get('/locations', (req, res) => {
  const alteredCakes = [];
  Cake.findAll({
    where: {
      lat: {
        [Op.ne]: 'null',
      },
      lon: {
        [Op.ne]: null,
      },
    }
  }).then(async cakes => {
    const promises = [];
    cakes.forEach(cake => {
      promises.push(Rating.findAll({
        where: {
          cake_id: cake.id
        }
      }).then(ratings => {
        let alteredCake = {};
        if (cake) {
          alteredCake = JSON.parse(JSON.stringify(cake, null, 2));
          alteredCake.ratings = JSON.parse(JSON.stringify(ratings, null, 2));
          alteredCakes.push(alteredCake);
        }
      }))
    });
    await Promise.all(promises);
    res.status(200).send(alteredCakes);
  });
});

router.post('/:id/rate', (req, res, next) => {
  const input = req.body;
  Rating.upsert({
    user_id: input.user_id,
    rating: input.rating,
    comment: input.comment,
    cake_id: req.params.id
  }).then(() => {
    res.status(200).send();
  });
});

module.exports = router;
