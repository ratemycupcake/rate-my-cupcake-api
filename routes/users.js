const express = require('express');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const md5 = require('md5');
const fetchUrl = require("fetch").fetchUrl;
const User = require('../models/user');
const Cake = require('../models/cake');
const Favourite = require('../models/favourite');
const router = express.Router();

const saltRounds = 10;
const key = '3F039A82-828A-40A0-9DB0-967FEF0B1EB0';

router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

function CheckPasswordValid(password) {
  const rulesFuncs = {
    min: v => v.length >= 8 || 'Min 8 characters',
  };

  const rules = [
    rulesFuncs.min
  ];

  let errored = false;
  for (let i = 0; i < rules.length; i += 1) {
    const internal = rules[i](password);
    console.log(internal, password, rules[i](password), rules[i]);
    errored = internal != true;
  }
  return !errored;
}

async function ConfirmUniqueEmail(email) {
  const user = await User.findOne({where: {email}});
  return !user;
}

async function ConfirmUniqueDisplayName(display_name) {
  const user = await User.findOne({where: {display_name}});
  return !user;
}

router.post('/signup', async (req, res) => {
  const input = req.body;
  // Check password is in a good format
  if (CheckPasswordValid(input.password)
    && await ConfirmUniqueEmail(input.email)
    && await ConfirmUniqueDisplayName(input.display_name)) {
    bcrypt.genSalt(saltRounds, (err, salt) => {
      bcrypt.hash(input.password, salt, (err, hash) => {
        fetchUrl(`https://en.gravatar.com/${md5(input.email)}.json`, (error, meta, data) => {
          let avatar = '';
          if (data) {
            parsedData = JSON.parse(data.toString());
            if (parsedData.entry) {
              avatar = parsedData.entry[0].thumbnailUrl;
            }
          }
          User.create({
            email: input.email,
            display_name: input.display_name,
            password: hash,
            avatar
          });
          res.status(200).send({});
        });
      });
    });
  } else {
    const errors = {
      password: CheckPasswordValid(input.password),
      email: await ConfirmUniqueEmail(input.email),
      name: await ConfirmUniqueDisplayName(input.display_name),
    };
    res.status(200).send({errors});
  }
});

router.post('/login', (req, res, next) => {
  const input = req.body;
  
  User.findOne({
    where: {
      email: input.email
    }
  }).then(user => {
    if (user) {
      bcrypt.compare(input.password, user.password, (err, out) => {
        if (out) {
          const safeUser = {};
          safeUser.id = user.id;
          safeUser.display_name = user.display_name;
          safeUser.avatar = user.avatar;
          jwt.sign({safeUser}, key, { expiresIn: '8h' }, (err, token) => {
            res.status(200).json({
              message: 'Success',
              token,
              safeUser,
            });
          });
        } else {
          res.status(200).json({
            message: 'Error'
          });
        }
      })
    } else {
      res.status(200).json({
        message: 'Error'
      });
    }
  })
});

router.post('/auto_login', (req, res, next) => {
  const input = req.body;
  jwt.verify(input.jwt, key, (err, decoded) => {
    if (decoded) {
      return res.status(200).send(decoded.safeUser);
    }
    return res.status(200).send({});
  });
});

router.post('/profile', verifyToken, (req, res, next) => {
  const input = req.body;
  User.findOne({
    where: {
      id: input.user_id
    },
    include: [
      {
        model: Favourite,
        include: [
          {
            model: Cake
          }
        ]
      }
    ]
  }).then(user => {
    user.password = undefined;
    res.status(200).send(user);
  });
});

router.post('/password', (req, res, next) => {
  const input = req.body;
  if (CheckPasswordValid(input.password)) {
    bcrypt.genSalt(saltRounds, (err, salt) => {
      bcrypt.hash(input.password, salt, (err, hash) => {
        User.update({
          password: hash
        }, {
          where: {
            id: input.user
          }
        });
        res.status(200).send({});
      });
    });
  } else {
    const errors = {
      password: CheckPasswordValid(input.password),
    };
    res.status(200).send({errors});
  }
});

function verifyToken(req, res, next) {
  // const input = req.body;
  // jwt.verify(input.jwt, key, (err, decoded) => {
  //   if (decoded) {
  //     return next()
  //   }
  //   return res.status(401).send({});
  // });
  next();
}

module.exports = router;
