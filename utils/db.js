const Sequelize = require('sequelize');
require('dotenv').config();
const assert = require('assert');
const Op = Sequelize.Op;
const Model = Sequelize.Model;

let _db;

const initDbConnection = (callback) => {
  if (_db) {
    return callback(null, _db);
  }

  _db = new Sequelize(
    'cupcake',
    process.env.DBUSER,
    process.env.DBPWD,
    {
      host: process.env.DBHOST,
      dialect: 'mysql'
    });

  _db
    .authenticate()
    .then(() => {
      console.log('Connection has been established successfully.');
    })
    .catch(err => {
      console.error('Unable to connect to the database:', err);
    });
  return callback(null, _db);
}

const getDbConnection = () => {
  assert.ok(_db, 'Database connection not yet initialised. Use initDbConnection first.');
  return _db;
}

module.exports = {
  initDbConnection,
  getDbConnection,
  Op,
  Model,
  Sequelize
};
